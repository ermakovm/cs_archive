﻿#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace da2lab5B
{
    class Program
    {
        //***********************************************//
        const int MAXN = 100;
        const int MAXM = 1000;
        const int INF = Int32.MaxValue;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static int n, m;
        static int[,] graph;
        static int[,] flow;
        static int[] colors;
        static int[] parents;
        static Queue<int> queue;
        //**********************************************//
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            mythread.Start();
#if DEBUG
            Console.WriteLine("Thread Start");
            sw.WriteLine("Thread Start");
#endif
            mythread.Join();
#if DEBUG
            Console.WriteLine("Thread Stop");
            sw.WriteLine("Thread Stop");
#endif

            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "maxflow";
#if DEBUG
            resout = ".debug";
#else
            resout = ".out";
#endif
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            n = tmp[0];
            m = tmp[1];
            graph = new int[n, n];
            flow = new int[n, n];
            colors = new int[n];
            parents = new int[n];
            queue = new Queue<int>();
        }

        static void solve()
        {
            for (int i = 0; i < m; i++)
            {
                int[] tmp = readInts();
                int a = tmp[0] - 1;
                int b = tmp[1] - 1;
                int c = tmp[2];
                graph[a, b] = c;
            }
            sw.WriteLine(max_flow(0, n - 1));
        }

        static bool bfs(int first, int last)
        {
            for (int i = 0; i < n; i++)
                colors[i] = 0;
            queue.Enqueue(first);
            colors[first] = 1;
            parents[first] = -1;
            while (queue.Count != 0)
            {
                int i = queue.Dequeue();
                colors[i] = 2;
                for (int j = 0; j < n; j++)
                {
                    if (colors[j] == 0 && graph[i, j] - flow[i, j] > 0)
                    {
                        queue.Enqueue(j);
                        colors[j] = 1;
                        parents[j] = i;
                    }
                }
            }
            return colors[last] == 2;
        }

        static int max_flow(int start, int finish)
        {
            int max_flow = 0;
            while (bfs(start, finish))
            {
                int increment = INF;
                for (int u = n - 1; parents[u] >= 0; u = parents[u])
                {
                    increment = Math.Min(increment, graph[parents[u], u] - flow[parents[u], u]);
                }
                for (int u = n - 1; parents[u] >= 0; u = parents[u])
                {
                    flow[parents[u], u] += increment;
                    flow[u, parents[u]] -= increment;
                }
                max_flow += increment;
            }
            return max_flow;
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
#if DEBUG
            Console.WriteLine("ReadInts");
            sw.WriteLine("ReadInts");
#endif
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
#if DEBUG
                Console.WriteLine(res[i]);
                sw.WriteLine(res[i]);
#endif
            }
            return res;
        }

        static string[] readWords()
        {
            string[] tmp = sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
#if DEBUG
            Console.WriteLine("ReadWord");
            sw.WriteLine("ReadWord");
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                sw.WriteLine(tmp[i]);
            }
#endif
            return tmp;
        }
    }

    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public void setPair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public void setFirst(E first = default(E))
        {
            this.first = first;
        }

        public void setSecond(K second = default(K))
        {
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

#if DEBUG
        public void printPair()
        {
            Console.WriteLine(getFirst() + " " + getSecond());
        }
#endif
    }
}