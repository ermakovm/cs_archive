﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace da2lab1E
{
    class Program
    {
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static int n, m;
        static List<int>[] g;
        static int[] cl;
        static int[] p;
        static int cycle_st, cycle_end;
        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            Thread mythread = new Thread(Th, 1000000000);
            mythread.Start();
            mythread.Join();
            sr.Close();
            sw.Close();
        }

        static void Th()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "cycle";
            resout = ".out";
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            n = tmp[0];
            m = tmp[1];
            g = new List<int>[n + 5];
            cl = new int[n + 5];
            p = new int[n + 5];
            for (int i = 0; i < n + 5; i++)
                g[i] = new List<int>();

        }

        static void solve()
        {
            for (int i = 0; i < m; i++)
            {
                int[] tmp = readInts();
                g[tmp[0]].Add(tmp[1]);
            }
            for (int i = 0; i < n + 5; i++)
            {
                p[i] = -1;
            }
            cycle_st = -1;
            for (int i = 0; i < n; i++)
                if (dfs(i))
                    break;
            if (cycle_st == -1)
            {
                sw.WriteLine("NO");

            }
            else
            {
                sw.WriteLine("YES");
                //sw.WriteLine(cycle_st + " " + cycle_end);
                List<int> cycle = new List<int>();
                cycle.Add(cycle_st);
                for (int v = cycle_end; v != cycle_st; v = p[v])
                {
                    cycle.Add(v);
                }
                //cycle.Add(cycle_st);
                cycle.Reverse();
                for (int i = 0; i < cycle.Count; i++)
                {
                    sw.Write((cycle[i]).ToString() + " ");
                }
            }

        }

        static bool dfs(int v)
        {
            cl[v] = 1;
            for (int i = 0; i < g[v].Count; ++i)
            {
                int to = g[v][i];
                if (cl[to] == 0)
                {
                    p[to] = v;
                    if (dfs(to))
                        return true;
                }
                else if (cl[to] == 1)
                {
                    cycle_end = v;
                    cycle_st = to;
                    return true;
                }
            }
            cl[v] = 2;
            return false;
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
            }
            return res;
        }

        static string[] readWords()
        {
            return sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}