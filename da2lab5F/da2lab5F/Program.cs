﻿#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace da2lab5F
{
    class Program
    {
        //***********************************************//
        const int MAXN = 100;
        const int MAXM = 1000;
        const Int64 INF = Int64.MaxValue;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static int n;
        static int m;
        static int[] verties;
        static int[] parent;
        static Pair<int, int>[] edges;
        static List<Edge>[] graph;
        static Queue<int> bfs_queue;
        //**********************************************//
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            mythread.Start();
#if DEBUG
            Console.WriteLine("Thread Start");
            sw.WriteLine("Thread Start");
#endif
            mythread.Join();
#if DEBUG
            Console.WriteLine("Thread Stop");
            sw.WriteLine("Thread Stop");
#endif

            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "circulation";
#if DEBUG
            resout = ".debug";
#else
            resout = ".out";
#endif
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            n = tmp[0];
            m = tmp[1];
            bfs_queue = new Queue<int>();
            graph = new List<Edge>[n + 2];
            verties = new int[n + 2];
            parent = new int[n + 2];
            edges = new Pair<int, int>[m];
            for (int i = 0; i < n + 2; i++)
            {
                graph[i] = new List<Edge>();
            }
        }

        static void solve()
        {
            for (int i = 0; i < m; i++)
            {
                int[] tmp = readInts();
                int x = tmp[0];
                int y = tmp[1];
                int z = tmp[2];
                int q = tmp[3];
                edges[i] = updateGraph(x, y, q - z, z);
                updateGraph(0, y, z, z);
                updateGraph(x, n + 1, z, z);
            }
            while (bfs())
            {
                for (int i = 0; i <= n + 1; i++)
                    parent[i] = 0;
                while (dfs(0, Int32.MaxValue) != 0)
                {
                }
            }
            bool flag = true;
            for (int i = 0; i < graph[0].Count; i++)
                if (graph[0].ElementAt(i).F < graph[0].ElementAt(i).C)
                {
                    sw.WriteLine("NO");
                    flag = false;
                    break;
                }
            if (flag)
            {
                sw.WriteLine("YES");
                for (int i = 0; i < m; i++)
                    sw.WriteLine(graph[edges[i].getFirst()].ElementAt(edges[i].getSecond()).F + graph[edges[i].getFirst()].ElementAt(edges[i].getSecond()).L);

            }
        }

        static Pair<int, int> updateGraph(int from, int to, Int64 c, Int64 l)
        {
            graph[from].Add(new Edge(to, l, c, 0, graph[to].Count));
            graph[to].Add(new Edge(from, l, 0, 0, graph[from].Count - 1));
            return new Pair<int, int>(from, graph[from].Count - 1);
        }

        static bool bfs()
        {
            bfs_queue.Clear();
            bfs_queue.Enqueue(0);
            for (int i = 0; i <= n + 1; i++)
                verties[i] = -1;
            verties[0] = 0;
            while (bfs_queue.Count != 0)
            {
                int u = bfs_queue.Dequeue();
                for (int i = 0; i < graph[u].Count; i++)
                    if (verties[graph[u].ElementAt(i).To] == -1 && graph[u].ElementAt(i).F < graph[u].ElementAt(i).C)
                    {
                        bfs_queue.Enqueue(graph[u].ElementAt(i).To);
                        verties[graph[u].ElementAt(i).To] = verties[u] + 1;
                    }
            }
            return verties[n + 1] != -1;
        }

        static Int64 dfs(int start, Int64 flow)
        {
            if (flow == 0)
            {
                return 0;
            }
            else if (start == n + 1)
            {
                return flow;
            }
            else
            {
                for (int i = parent[start]++; i < graph[start].Count; i++)
                {
                    if (verties[graph[start].ElementAt(i).To] != verties[start] + 1) continue;
                    Int64 pushed = dfs(graph[start].ElementAt(i).To, Math.Min(flow, graph[start].ElementAt(i).C - graph[start].ElementAt(i).F));
                    if (pushed > 0)
                    {
                        graph[start].ElementAt(i).F += pushed;
                        graph[graph[start].ElementAt(i).To].ElementAt(graph[start].ElementAt(i).Where).F -= pushed;
                        return pushed;
                    }
                }
                return 0;
            }
        }

        public class Edge
        {
            private int to, where;
            private Int64 l, c, f;

            public Edge(int to = 0, Int64 l = 0, Int64 c = 0, Int64 f = 0, int where = 0)
            {
                this.to = to;
                this.l = l;
                this.c = c;
                this.f = f;
                this.where = where;
            }

            public Int64 L
            {
                get { return l; }
                set { l = value; }
            }

            public Int64 C
            {
                get { return c; }
                set { c = value; }
            }

            public Int64 F
            {
                get { return f; }
                set { f = value; }
            }
            public int To
            {
                get { return to; }
                set { to = value; }
            }

            public int Where
            {
                get { return where; }
                set { where = value; }
            }


        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
#if DEBUG
            Console.WriteLine("ReadInts");
            sw.WriteLine("ReadInts");
#endif
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
#if DEBUG
                Console.WriteLine(res[i]);
                sw.WriteLine(res[i]);
#endif
            }
            return res;
        }

        static string[] readWords()
        {
            string[] tmp = sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
#if DEBUG
            Console.WriteLine("ReadWord");
            sw.WriteLine("ReadWord");
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                sw.WriteLine(tmp[i]);
            }
#endif
            return tmp;
        }
    }

    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public void setPair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public void setFirst(E first = default(E))
        {
            this.first = first;
        }

        public void setSecond(K second = default(K))
        {
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

#if DEBUG
        public void printPair()
        {
            Console.WriteLine(getFirst() + " " + getSecond());
        }
#endif
    }
}