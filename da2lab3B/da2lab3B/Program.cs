﻿#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
namespace da2lab3B
{
    class Program
    {
        //***********************************************//
        const int MINN = 1;
        const int MAXN = 2000;
        const int INF = 1000000000;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static public StreamWriter sw;
        //**********************************************//
        static int n, s, f, m;
        static List<Edge> graph;
        static int[,] matrix;
        static int[] d;
        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.Start();
#if DEBUG
            Console.WriteLine("Thread Start");
            sw.WriteLine("Thread Start");
#endif
            mythread.Join();
#if DEBUG
            Console.WriteLine("Thread Stop");
            sw.WriteLine("Thread Stop");
#endif

            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "pathmgep";
#if DEBUG
            resout = ".debug";
#else
            resout=".out";
#endif
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            n = tmp[0];
            s = tmp[1] - 1;
            f = tmp[2] - 1;
            m = 0;
            d = new int[n];
            graph = new List<Edge>();
            matrix = new int[n, n];
        }

        static void solve()
        {
            for (int i = 0; i < n; i++)
            {
                int[] tmp = readInts();
                for (int j = 0; j < n; j++)
                {
                    if (i != j && tmp[j] != -1)
                    {
                        graph.Add(new Edge(i, j, tmp[j]));
                        m++;
                    }
                }
            }
            fordBellman(s);
            if (d[f] == INF)
                sw.WriteLine(-1);
            else
                sw.WriteLine(d[f]);
        }

        static void fordBellman(int st)
        {
            for (int i = 0; i < n; i++)
            {
                d[i] = INF;
            }
            d[st] = 0;
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    if (d[graph[j].getStart()] < INF)
                    {
                        d[graph[j].getFinish()] = Math.Min(d[graph[j].getFinish()], d[graph[j].getStart()] + graph[j].getCost());
                    }
                }
            }
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
#if DEBUG
            Console.WriteLine("ReadInts");
            sw.WriteLine("ReadInts");
#endif
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
#if DEBUG
                Console.WriteLine(res[i]);
                sw.WriteLine(res[i]);
#endif
            }
            return res;
        }

        static string[] readWords()
        {
            string[] tmp = sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
#if DEBUG
            Console.WriteLine("ReadWord");
            sw.WriteLine("ReadWord");
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                sw.WriteLine(tmp[i]);
            }
#endif
            return tmp;
        }
    }

    class Edge
    {
        private int start;
        private int finish;
        private int cost;

        public Edge()
        {
            start = 0;
            finish = 0;
            cost = 0;
        }

        public Edge(int s = 0, int f = 0, int c = 0)
        {
            start = s;
            finish = f;
            cost = c;
        }

        public int getStart()
        {
            return start;
        }

        public int getFinish()
        {
            return finish;
        }

        public int getCost()
        {
            return cost;
        }
    }

    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public void setPair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public void setFirst(E first = default(E))
        {
            this.first = first;
        }

        public void setSecond(K second = default(K))
        {
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

#if DEBUG
        public void printPair()
        {
            Console.WriteLine(getFirst() + " " + getSecond());

        }
#endif
    }
}