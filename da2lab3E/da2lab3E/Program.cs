﻿#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
namespace da2lab3E
{
    class Program
    {
        //***********************************************//
        const int MINN = 2;
        const int MAXN = 2000;
        const int MINM = 1;
        const int MAXM = 5000;
        const Int64 INF = Int64.MaxValue - 1;
        const Int64 MINF = Int64.MinValue + 1;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static public StreamWriter sw;
        //**********************************************//
        static Int64 n, s, m;
        static List<Edge> graph;
        static int[,] matrix;
        static Int64[] d, p;
        static string[] res;
        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.Start();
#if DEBUG
            Console.WriteLine("Thread Start");
            sw.WriteLine("Thread Start");
#endif
            mythread.Join();
#if DEBUG
            Console.WriteLine("Thread Stop");
            sw.WriteLine("Thread Stop");
#endif

            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "path";
#if DEBUG
            resout = ".debug";
#else
            resout = ".out";
#endif
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            Int64[] tmp = readInts();
            n = tmp[0];
            m = tmp[1];
            s = tmp[2] - 1;
            d = new Int64[n];
            p = new Int64[n];
            graph = new List<Edge>();
        }

        static void solve()
        {
            for (int i = 0; i < m; i++)
            {
                Int64[] tmp = readInts();
                Int64 a = tmp[0] - 1;
                Int64 b = tmp[1] - 1;
                Int64 c = tmp[2];
                graph.Add(new Edge(a, b, c));
            }
            fordBellman();
            int l = 0;
            for (int i = 0; i < n; i++)
            {
                if (d[i] == MINF)
                    sw.WriteLine("-");
                else if (d[i] == INF)
                    sw.WriteLine("*");
                else
                    sw.WriteLine(d[i]);
            }
        }

        static void fordBellman()
        {
            for (int i = 0; i < n; i++)
                d[i] = INF;
            d[s] = 0;
            for (int i = 0; i < n - 1; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Int64 a = graph[j].getStart();
                    Int64 b = graph[j].getFinish();
                    Int64 c = graph[j].getCost();
                    if (d[a] != INF && d[b] > d[a] + c)
                    {
                        d[b] = d[a] + c;
                    }
                }
            }
            Int64[] d2 = new Int64[n];
            for (int i = 0; i < n; i++)
                d2[i] = d[i];
            for (int i = 0; i < m; i++)
            {
                Int64 a = graph[i].getStart();
                Int64 b = graph[i].getFinish();
                Int64 c = graph[i].getCost();
                if (d[a] != INF && d[b] > d[a] + c)
                {
                    d[b] = d[a] + c;
                }
            }
            for (int i = 0; i < n; i++)
            {
                if (d[i] != d2[i])
                    d[i] = MINF;
            }
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    Int64 a = graph[j].getStart();
                    Int64 b = graph[j].getFinish();
                    Int64 c = graph[j].getCost();
                    if (d[a] == MINF)
                        d[b] = MINF;
                }
            }
        }        

        static Int64[] readInts()
        {
            string[] temp = readWords();
            Int64[] res = new Int64[temp.Length];
#if DEBUG
            Console.WriteLine("ReadInts");
            sw.WriteLine("ReadInts");
#endif
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt64(temp[i]);
#if DEBUG
                Console.WriteLine(res[i]);
                sw.WriteLine(res[i]);
#endif
            }
            return res;
        }

        static string[] readWords()
        {
            string[] tmp = sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
#if DEBUG
            Console.WriteLine("ReadWord");
            sw.WriteLine("ReadWord");
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                sw.WriteLine(tmp[i]);
            }
#endif
            return tmp;
        }
    }

    class Edge
    {
        private Int64 start;
        private Int64 finish;
        private Int64 cost;

        public Edge()
        {
            start = 0;
            finish = 0;
            cost = 0;
        }

        public Edge(Int64 s = 0, Int64 f = 0, Int64 c = 0)
        {
            start = s;
            finish = f;
            cost = c;
        }

        public Int64 getStart()
        {
            return start;
        }

        public Int64 getFinish()
        {
            return finish;
        }

        public Int64 getCost()
        {
            return cost;
        }
    }

    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public void setPair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public void setFirst(E first = default(E))
        {
            this.first = first;
        }

        public void setSecond(K second = default(K))
        {
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

#if DEBUG
        public void printPair()
        {
            Console.WriteLine(getFirst() + " " + getSecond());
  
        }
#endif
    }
}