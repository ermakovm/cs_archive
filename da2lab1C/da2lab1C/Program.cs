﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace da2lab1C
{
    class Program
    {
        //***********************************************//
        const int INF = Int32.MaxValue;
        const int MAXN = 100010;
        const int MAXM = 200020;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static int n, m, v1, v2;
        static List<Pair>[] g;
        static List<int> d, id, p;
        static LinkedList<int> q;
        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.Start();
            mythread.Join();
            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "shortpath";
            resout = ".out";
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            n = tmp[0];
            m = tmp[1];
            v1 = tmp[2] - 1;
            v2 = tmp[3] - 1;
            g = new List<Pair>[n];
            for (int i = 0; i < n; i++)
                g[i] = new List<Pair>();

        }

        static void solve()
        {
            for (int i = 0; i < m; i++)
            {
                int[] tmp = readInts();
                Pair pair = new Pair(tmp[1] - 1, tmp[2]);
                g[tmp[0] - 1].Add(pair);
            }
            d = new List<int>();
            for (int i = 0; i < n; i++)
                d.Add(INF);
            d[v1] = 0;
            id = new List<int>();
            for (int i = 0; i < n; i++)
                id.Add(default(int));
            q = new LinkedList<int>();
            q.AddLast(v1);
            p = new List<int>();
            for (int i = 0; i < n; i++)
                p.Add(-1);
            while (q.Count != 0)
            {
                int v = q.First();
                q.RemoveFirst();
                id[v] = 1;
                for (int i = 0; i < g[v].Count; i++)
                {
                    int to = g[v][i].getA();
                    int len = g[v][i].getB();
                    if (d[to] > d[v] + len)
                    {
                        d[to] = d[v] + len;
                        if (id[to] == 0)
                            q.AddLast(to);
                        else if (id[to] == 1)
                            q.AddFirst(to);
                        p[to] = v;
                        id[to] = 1;
                    }
                }
            }
            int lens = d[v2];
            if (lens == INF)
                sw.WriteLine("Unreachable");
            else
                sw.WriteLine(lens);
        }

        class Pair
        {
            private int a;
            private int b;

            public Pair()
            {
                a = default(int);
                b = default(int);
            }

            public Pair(int a, int b)
            {
                this.a = a;
                this.b = b;
            }

            public int getA()
            {
                return a;
            }

            public int getB()
            {
                return b;
            }

            public void setA(int a)
            {
                this.a = a;
            }

            public void setB(int b)
            {
                this.b = b;
            }
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
            }
            return res;
        }

        static string[] readWords()
        {
            return sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}