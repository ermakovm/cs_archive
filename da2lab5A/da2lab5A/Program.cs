﻿#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace da2lab5A
{
    class Program
    {
        //***********************************************//
        const int MAXN = 200;
        const int MAXM = 200;
        const int MAXK = MAXN * MAXM;
        const int INF = Int32.MaxValue;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;

        static public StreamWriter sw;
        //**********************************************//
        static int n, m, k;
        static List<int>[] graph;
        static int result;
        static bool[] used;
        static int[] verts;
        static bool[] used_2;
        //**********************************************//
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            mythread.Start();
#if DEBUG
            Console.WriteLine("Thread Start");
            sw.WriteLine("Thread Start");
#endif
            mythread.Join();
#if DEBUG
            Console.WriteLine("Thread Stop");
            sw.WriteLine("Thread Stop");
#endif

            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "matching";
#if DEBUG
            resout = ".debug";
#else
            resout = ".out";
#endif
            resin = ".in";
            ch = new char[] { ' ', '\t', '\n', '\r' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            n = tmp[0];
            m = tmp[1];
            k = tmp[2];
            graph = new List<int>[n];
            Parallel.For(0, n, i => { graph[i] = new List<int>(); });
            used = new bool[n];
            verts = new int[m];
            result = 0;
        }

        static void solve()
        {
            for (int i = 0; i < k; i++)
            {
                int[] tmp = readInts();
                int a = tmp[0] - 1;
                int b = tmp[1] - 1;
                graph[a].Add(b);
            }
            used_2 = new bool[n];


            for (int i = 0; i < m; i++)
                verts[i] = -1;
            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < graph[i].Count; j++)
                {
                    if (verts[graph[i][j]] == -1)
                    {
                        verts[graph[i][j]] = i;
                        used_2[i] = true;
                        break;
                    }
                }
            }

            for (int i = 0; i < n; i++)
            {
                if (used_2[i])
                    continue;
                for (int j = 0; j < n; j++)
                    used[j] = false;
                dfs(i);
            }

            for (int i = 0; i < m; ++i)
                if (verts[i] != -1)
                    result++;
            sw.WriteLine(result);
        }

        static bool dfs(int v)
        {
            if (used[v]) return false;
            used[v] = true;
            for (int i = 0; i < graph[v].Count; ++i)
            {
                int to = graph[v][i];
                if (verts[to] == -1 || dfs(verts[to]))
                {
                    verts[to] = v;
                    return true;
                }
            }
            return false;
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
#if DEBUG
            Console.WriteLine("ReadInts");
            sw.WriteLine("ReadInts");
#endif
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
#if DEBUG
                Console.WriteLine(res[i]);
                sw.WriteLine(res[i]);
#endif
            }
            return res;
        }

        static string[] readWords()
        {
            string[] tmp = sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
#if DEBUG
            Console.WriteLine("ReadWord");
            sw.WriteLine("ReadWord");
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                sw.WriteLine(tmp[i]);
            }
#endif
            return tmp;
        }
    }

    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public void setPair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public void setFirst(E first = default(E))
        {
            this.first = first;
        }

        public void setSecond(K second = default(K))
        {
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

#if DEBUG
        public void printPair()
        {
            Console.WriteLine(getFirst() + " " + getSecond());
        }
#endif
    }
}
