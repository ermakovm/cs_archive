﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace da2lab2B
{
    class Program
    {
        //***********************************************//
        const int MAXN = 100000;
        const int MAXM = 200000;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static List<Pair>[] graph;
        static bool[] use;
        static int timer, n, m;
        static int[] tin, fup;
        static List<int> res;
        static int count;
        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.Start();
            mythread.Join();
            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "bridges";
            resout = ".out";
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            n = tmp[0];
            m = tmp[1];
            graph = new List<Pair>[n];
            res = new List<int>();
            use = new bool[n];
            for (int i = 0; i < n; i++)
            {
                graph[i] = new List<Pair>();
                use[i] = false;
            }
            timer = 0;
            count = 0;
            tin = new int[n];
            fup = new int[n];

        }

        static void solve()
        {
            for (int i = 1; i <= m; i++)
            {
                int[] tmp = readInts();
                int a = tmp[0] - 1;
                int b = tmp[1] - 1;
                graph[a].Add(new Pair(b, i));
                graph[b].Add(new Pair(a, i));
            }
            for (int i = 0; i < n; i++)
                if (!use[i])
                    dfs(i);
            sw.WriteLine(count);
            res.Sort();
            for (int i = 0; i < res.Count; i++)
                sw.Write(res[i] + " ");
        }

        static void dfs(int v, int p = -1)
        {
            use[v] = true;
            tin[v] = fup[v] = timer++;
            for (int i = 0; i < graph[v].Count; i++)
            {
                int to = graph[v][i].getA();
                if (to == p)
                { continue; }
                if (use[to])
                    fup[v] = Math.Min(fup[v], tin[to]);
                else
                {
                    dfs(to, v);
                    fup[v] = Math.Min(fup[v], fup[to]);
                    if (fup[to] > tin[v])
                    {
                        count++;
                        for(int k=0;k<graph[v].Count;k++)
                        {
                            if (graph[v][k].getA() == to)
                            {
                                res.Add(graph[v][k].getB());
                                break;
                            }
                        }
                    }
                }
            }
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
            }
            return res;
        }

        static string[] readWords()
        {
            return sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
        }

        class Pair
        {
            private int a;
            private int b;

            public Pair()
            {
                a = default(int);
                b = default(int);
            }

            public Pair(int a, int b)
            {
                this.a = a;
                this.b = b;
            }

            public int getA()
            {
                return a;
            }

            public int getB()
            {
                return b;
            }

            public void setA(int a)
            {
                this.a = a;
            }

            public void setB(int b)
            {
                this.b = b;
            }
        }
    }
}
