﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace da2lab1A
{
    class Program
    {
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static int[] Color;
        static int N, M;
        static List<int>[] Edge;
        static Stack<int> stack;
        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            //init();
            //solve();
            Thread mythread = new Thread(Th, 100000000);
            mythread.Start();
            mythread.Join();
            sr.Close();
            sw.Close();
        }

        static void Th()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "topsort";
            resout = ".out";
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {            
            int[] tmp = readInts();
            N = tmp[0];
            M = tmp[1];
            Color = new int[N + 2];
            Edge = new List<int>[N + 2];
            for (int i = 0; i < Edge.Length; i++)
            {
                Edge[i] = new List<int>();
            }
            stack = new Stack<int>();

        }

        static void solve()
        {
            for (int i = 0; i < M; i++)
            {
                int[] tmp = readInts();
                if (Edge[tmp[0]] == null)
                {
                    Edge[tmp[0]] = new List<int>();
                }
                Edge[tmp[0]].Add(tmp[1]);
            }
            topsort();
        }

        static bool topsort()
        {
            bool Cycle;
            for (int i = 1; i <= N; i++)
            {
                Cycle = dfs(i);
                if (Cycle)
                {
                    sw.WriteLine("-1");
                    return false;
                }
            }
            for (int i = 1; i <= N; i++)
            {
                int t=stack.Pop();
                sw.Write(t + " ");                
            }
            return true;
        }

        static bool dfs(int v)
        {
            if (Color[v] == 1)
                return true;
            if (Color[v] == 2)
                return false;
            Color[v] = 1;
            for (int i = 0; i < Edge[v].Count; i++)
            {
                if (dfs(Edge[v][i]))
                    return true;
            }
            stack.Push(v);
            Color[v] = 2;
            return false;
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
            }
            return res;
        }

        static string[] readWords()
        {
            return sr.ReadLine().Split(ch);
        }
    }
}
