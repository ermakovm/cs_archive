﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace da2lab2D
{
    class Program
    {
        //***********************************************//
        const int MAXN = 100000;
        const int MAXM = 200000;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static List<int>[] graph;
        static int n, m;
        static int[] color;
        static bool ans;
        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.Start();
            mythread.Join();
            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "bipartite";
            resout = ".out";
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            n = tmp[0];
            m = tmp[1];
            graph = new List<int>[n];
            color = new int[n];
            Parallel.For(0, n, i =>
            {
                graph[i] = new List<int>();
                color[i] = 0;
            });
            ans = true;
        }

        static void solve()
        {
            for (int i = 0; i < m; i++)
            {
                int[] tmp = readInts();
                int a = tmp[0] - 1;
                int b = tmp[1] - 1;
                graph[a].Add(b);
                graph[b].Add(a);
            }
            ans = true;
            for (int i = 0; i < n; i++)
            {
                if (color[i] == 0)
                {
                    color[i] = 1;
                    dfs(i);
                }
            }
            if (ans)
                sw.WriteLine("YES");
            else
                sw.WriteLine("NO");
        }

        static void dfs(int v)
        {
            for (int i = 0; i < graph[v].Count; i++)
            {
                int to=graph[v][i];
                if (color[to] == 0)
                {
                    color[to] = 3 - color[v];
                    dfs(to);
                }
                else if (color[to] == color[v])
                    ans = false;
            }
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
            }
            return res;
        }

        static string[] readWords()
        {
            return sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
        }

        class Pair
        {
            private int a;
            private int b;

            public Pair()
            {
                a = default(int);
                b = default(int);
            }

            public Pair(int a, int b)
            {
                this.a = a;
                this.b = b;
            }

            public int getA()
            {
                return a;
            }

            public int getB()
            {
                return b;
            }

            public void setA(int a)
            {
                this.a = a;
            }

            public void setB(int b)
            {
                this.b = b;
            }
        }
    }
}
