﻿#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
namespace da2lab3D
{
    class Program
    {
        //***********************************************//
        const int MINN = 1;
        const int MAXN = 200;
        const int MINM = 0;
        const int MAXM = 10000;
        const int INF = 1000000000;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static public StreamWriter sw;
        //**********************************************//
        static int n, m, k;
        static List<Pair<int, int>>[] graph;
        static int[] d, id, p;
        static Queue<int> queue;
        static HashSet<Pair<int, int>> set;
        static LinkedList<int> q;
        static bool[] used;
        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.Start();
#if DEBUG
            Console.WriteLine("Thread Start");
            sw.WriteLine("Thread Start");
#endif
            mythread.Join();
#if DEBUG
            Console.WriteLine("Thread Stop");
            sw.WriteLine("Thread Stop");
#endif
            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "pathbgep";
#if DEBUG
            resout = ".debug";
#else
            resout = ".out";
#endif
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            n = tmp[0];
            m = tmp[1];
            k = 0;
            d = new int[n];
            id = new int[n];
            p = new int[n];
            q = new LinkedList<int>();
            graph = new List<Pair<int, int>>[n];
            set = new HashSet<Pair<int, int>>();
            Parallel.For(0, n, i =>
            {
                graph[i] = new List<Pair<int, int>>();
            });
            queue = new Queue<int>();
            used = new bool[n];
        }

        static void solve()
        {
            for (int i = 0; i < m; i++)
            {
                int[] tmp = readInts();
                int a = tmp[0] - 1;
                int b = tmp[1] - 1;
                int c = tmp[2];
                graph[a].Add(new Pair<int, int>(b, c));
                graph[b].Add(new Pair<int, int>(a, c));
            }

            int s = 0;
            for (int i = 0; i < n; i++)
            {
                d[i] = INF;
            }
            d[s] = 0;
            set.Add(new Pair<int, int>(d[s], s));
            while (set.Count != 0)
            {
                int v = set.First().getSecond();
                set.Remove(set.First());

                for (int j = 0; j < graph[v].Count; j++)
                {
                    int to = graph[v][j].getFirst();
                    int len = graph[v][j].getSecond();
                    if (d[v] + len < d[to])
                    {
                        set.Remove(new Pair<int, int>(d[to], to));
                        d[to] = d[v] + len;
                        p[to] = v;
                        set.Add(new Pair<int, int>(d[to], to));
                    }
                }
            }
            for (int i = 0; i < n; i++)
                sw.Write(d[i] + " ");
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
#if DEBUG
            Console.WriteLine("ReadInts");
            sw.WriteLine("ReadInts");
#endif
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
#if DEBUG
                Console.WriteLine(res[i]);
                sw.WriteLine(res[i]);
#endif
            }
            return res;
        }

        static string[] readWords()
        {
            string[] tmp = sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
#if DEBUG
            Console.WriteLine("ReadWord");
            sw.WriteLine("ReadWord");
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                sw.WriteLine(tmp[i]);
            }
#endif
            return tmp;
        }
    }

    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public void setPair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public void setFirst(E first = default(E))
        {
            this.first = first;
        }

        public void setSecond(K second = default(K))
        {
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

#if DEBUG
        public void printPair()
        {
            Console.WriteLine(getFirst() + " " + getSecond());

        }
#endif
    }
}