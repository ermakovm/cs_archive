#define _CRT_SECURE_NO_WARNINGS
#pragma comment(linker,"/STACK:1000000000")
#include <cstdio>
#include <iostream>
#include <set>
#include <vector>
#include <list>
#include <limits>
#include <queue>

long long min(long long a, long long b)
{
	if (a > b)
		return b;
	else
		return a;
}

const int INF = 2147483647;
const int MAXN = 205;
const int MAXM = 15005;

class Edge
{
public:
	int to, where;
	long long len, cap, flow;
public:
	Edge(int t, long long l, long long c, long long f, int w)
	{
		to = t;
		where = w;
		len = l;
		cap = c;
		flow = f;
	}

	Edge()
	{
		to = where = len = cap = flow = 0;
	}
};

int n, m, verties[MAXN], parent[MAXN];
std::pair<int, int>edges[MAXM];
std::vector<Edge>graph[MAXN];
std::queue<int> bfs_queue;

static bool bfs()
{
	bfs_queue.emplace(0);
	for (int i = 0; i <= n + 1; i++)
		verties[i] = -1;
	verties[0] = 0;
	while (bfs_queue.size() != 0)
	{
		int u = bfs_queue.front();
		bfs_queue.pop();
		for (int i = 0; i < graph[u].size(); i++)
		if (verties[graph[u][i].to] == -1 && graph[u][i].flow < graph[u][i].cap)
		{
			bfs_queue.push(graph[u][i].to);
			verties[graph[u][i].to] = verties[u] + 1;
		}
	}
	return verties[n + 1] != -1;
}

long long dfs(int start, long long flow)
{
	if (flow == 0)
		return 0;	
	else if (start == n + 1)
		return flow;
	else
	{
		for (int i = parent[start]++; i < graph[start].size(); i++)
		{
			if (verties[graph[start][i].to] != verties[start] + 1) continue;
			long long pushed = dfs(graph[start][i].to, min(flow, graph[start][i].cap - graph[start][i].flow));
			if (pushed > 0)
			{
				graph[start][i].flow += pushed;
				graph[graph[start][i].to][graph[start][i].where].flow -= pushed;
				return pushed;
			}
		}
		return 0;
	}
}

std::pair<int, int> updateGraph(int from, int to, long long cap, long long len)
{
	graph[from].push_back(Edge(to, len, cap, 0, graph[to].size()));
	graph[to].push_back(Edge(from, len, 0, 0, graph[from].size() - 1));
	return std::pair<int, int>(from, graph[from].size() - 1);
}

int main()
{
	freopen("circulation.in", "r", stdin);
	freopen("circulation.out", "w", stdout);
	std::cin >> n >> m;
	for (int i = 0; i < m; i++)
	{
		int x, y, z, q;
		std::cin >> x >> y >> z >> q;
		edges[i] = updateGraph(x, y, q - z, z);
		updateGraph(0, y, z, z);
		updateGraph(x, n + 1, z, z);
	}
	while (bfs())
	{
		for (int i = 0; i <= n + 1; i++)
			parent[i] = 0;
		while (dfs(0, INF) != 0)
		{
		}
	}
	bool flag = true;
	for (int i = 0; i < graph[0].size(); i++)
	if (graph[0][i].flow < graph[0][i].cap)
	{
		std::cout << "NO" << std::endl;
		flag = false;
		break;
	}
	if (flag)
	{
		std::cout << "YES" << std::endl;
		for (int i = 0; i < m; i++)
			std::cout<<graph[edges[i].first][edges[i].second].flow + graph[edges[i].first][edges[i].second].len<<std::endl;
	}
	return 0;
}