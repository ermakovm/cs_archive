#define _CRT_SECURE_NO_WARNINGS
#pragma comment(linker, "/STACK:10000000000")
#include <cstdio>
#include <iostream>
#include <vector>
#include <stack>

using namespace std;

#define MAXN 20000
#define MAXM 200000

int min(int a, int b)
{
	if (a < b)
		return a;
	else
		return b;
}

vector<int> graph[MAXN];
bool used[MAXN];
int colors[MAXN];
int n, m, maxcolor;
int timer;
stack<int> st;
int tin[MAXN], fup[MAXN];

void paint(int v, int color)
{
	colors[v] = color;
	for (int i = 0; i < graph[v].size(); i++)
	{
		int u = graph[v][i];
		if (colors[u] == 0)
		{
			if (tin[u] == fup[u])
			{
				maxcolor++;
				paint(u, maxcolor);
			}
			else
			{
				paint(u, color);
			}
		}
	}
}

void dfs(int v, int p = -1)
{
	used[v] = true;
	tin[v] = fup[v] = timer++;
	for (int i = 0; i < graph[v].size(); i++)
	{
		int to = graph[v][i];
		if (to == p)  continue;
		if (used[to])
			fup[v] = min(fup[v], tin[to]);
		else 
		{
			dfs(to, v);
			fup[v] = min(fup[v], fup[to]);
			//if (fup[to] > tin[v])
				//continue;
		}
	}
}

int main()
{
	freopen("bicone.in", "r", stdin);
	freopen("bicone.out", "w", stdout);
	cin >> n >> m;
	for (int i = 0; i < m; i++)
	{
		int a, b;
		cin >> a >> b;
		graph[a].push_back(b);
		graph[b].push_back(a);
	}

	for (int i = 0; i < n; i++)
		colors[i] = 0;
	maxcolor = 0;
	timer = 0;
	for (int i = 0; i < n; i++)
	{
		if (!used[i])
			dfs(i);
	}
	for (int i = 0; i < n; i++)
	{
		if (colors[i] == 0)
		{
			maxcolor++;
			paint(i, maxcolor);
		}
	}


	cout << maxcolor << endl;
	for (int i = 0; i < n; i++)
		cout << colors[i] << " ";
	cout << endl;
	return 0;
}