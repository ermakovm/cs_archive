﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace da2lab2F
{
    class Program
    {
        //***********************************************//
        const int MAXN = 100000;
        const int MAXM = 200000;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static List<Pair<int, int>>[] graph;
        static int n, m, time, cur;
        static int[] color, enter, ret;
        static bool[] used;
        static Stack<int> stack;
        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.Start();
            mythread.Join();
            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "biconv";
            resout = ".out";
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            n = tmp[0];
            m = tmp[1];
            graph = new List<Pair<int, int>>[n];
            color = new int[m];
            enter = new int[n];
            ret = new int[n];
            time = 0;
            Parallel.For(0, n, i =>
            {
                graph[i] = new List<Pair<int, int>>();
                enter[i] = 0;
                ret[i] = 0;
            });
            used = new bool[n];
            stack = new Stack<int>();
        }

        static void solve()
        {
            for (int i = 0; i < m; i++)
            {
                int[] tmp = readInts();
                int a = tmp[0] - 1;
                int b = tmp[1] - 1;
                graph[a].Add(new Pair<int, int>(b, i));
                graph[b].Add(new Pair<int, int>(a, i));
            }

            for(int i=0;i<n;i++)
            {
                if(!used[i])
                {
                    time = 0;
                    dfs(i, -1);
                }
            }
            sw.WriteLine(cur);
            for(int i=0;i<m;i++)
            {
                sw.Write(color[i] + " ");
            }
        }

        static void dfs(int v, int p)
        {
            enter[v] = ret[v] = time++;
            used[v] = true;
            for (int i = 0; i < graph[v].Count; i++)
            {
                int u = graph[v][i].getFirst();
                if (u == p)
                    continue;
                if (!used[u])
                {
                    stack.Push(graph[v][i].getSecond());
                    dfs(u, v);
                    if (ret[u] >= enter[v])
                    {
                        cur++;
                        while (stack.Peek() != graph[v][i].getSecond())
                        {
                            color[stack.Pop()] = cur;
                        }
                        color[graph[v][i].getSecond()] = cur;
                        stack.Pop();
                    }
                    if (ret[u] < ret[v])
                        ret[v] = ret[u];
                }
                else
                {
                    if (enter[u] < enter[v])
                        stack.Push(graph[v][i].getSecond());
                    if (ret[v] > enter[u])
                        ret[v] = ret[u];
                }
            }
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
            }
            return res;
        }

        static string[] readWords()
        {
            return sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
        }


    }
    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair()
        {
            first = default(E);
            second = default(K);
        }

        public Pair(E first, K second)
        {
            this.first = first;
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

        public void setFirst(E first)
        {
            this.first = first;
        }

        public void setSecond(K second)
        {
            this.second = second;
        }
    }
}
