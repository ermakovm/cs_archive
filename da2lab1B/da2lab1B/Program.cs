﻿////////https://sites.google.com/site/bydlocode/graphs/condensation-tarjan
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;

namespace da2lab1B
{
    class Program
    {
        //***********************************************//
        const int MAXN = 20004;
        const int MAXM = 200005;

        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static int N, M;
        static int[] e, es, eb;
        static int ed;

        static int[] pinst, tin, hh, stack;
        static int sd, tm;
        static int[] cond;
        static int cn;

        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.Start();
            mythread.Join();
            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "cond";
            resout = ".out";
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            N = tmp[0];
            M = tmp[1];
            e = new int[MAXM];
            es = new int[MAXM];
            eb = new int[MAXN];
            pinst = new int[MAXN];
            tin = new int[MAXN];
            hh = new int[MAXN];
            stack = new int[MAXN];
            cond = new int[MAXN];
        }

        static void solve()
        {
            for (int i = 0; i < M; i++)
            {
                int[] tmp = readInts();
                int a = tmp[0] - 1;
                int b = tmp[1] - 1;
                add(a, b);
            }
            for (int i = 0; i < N; i++)
            {
                if (tin[i] == default(int))
                    dfs(i);
            }
            sw.WriteLine(cn);
            for (int i = 0; i < N; i++)
                sw.Write(cn + 1 - cond[i] + " ");
        }

        static void add(int v, int u)
        {
            ed++;
            e[ed] = u;
            es[ed] = eb[v];
            eb[v] = ed;
        }

        static void dfs(int v)
        {
            tm++;
            sd++;
            tin[v] = tm;
            hh[v] = tm;
            pinst[v] = sd;
            stack[sd] = v;
            int i, k;
            for (k = eb[v]; k != default(int); k = es[k])
            {
                i = e[k];
                if (tin[i] == default(int))
                    dfs(i);
                if (pinst[i] > 0)
                    hh[v] = Math.Min(hh[v], hh[i]);
            }

            if (hh[v] == tin[v])
            {
                cn++;
                do
                {
                    i = stack[sd];
                    pinst[i] = 0;
                    cond[i] = cn;
                    sd--;
                } while (i != v);
            }
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
            }
            return res;
        }

        static string[] readWords()
        {
            return sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
