#define _CRT_SECURE_NO_WARNINGS
#pragma comment(linker, "/STACK:10000000000")
#include <cstdio>
#include <iostream>
#include <set>
#include <vector>

const int INF = 1000000000;
const int MAXN = 30000;
const int MAXM = 400000;
const int s = 0;
int n, m;
std::vector<std::vector<std::pair<int, int>>>graph(MAXN);

int main()
{
	freopen("pathbgep.in", "r", stdin);
	freopen("pathbgep.out", "w", stdout);
	std::cin >> n >> m;
	for (int i = 0; i < m; i++)
	{
		int a, b, c;
		std::cin >> a >> b >> c;
		graph[a - 1].push_back(std::make_pair(b - 1, c));
		graph[b - 1].push_back(std::make_pair(a - 1, c));
	}

	std::vector<int>d(n, INF), p(n);
	d[s] = 0;
	std::set<std::pair<int, int>>set;
	set.insert(std::make_pair(d[s], s));
	while (!set.empty())
	{
		int v = set.begin()->second;
		set.erase(set.begin());

		for (size_t j = 0; j < graph[v].size(); j++)
		{
			int to = graph[v][j].first;
			int len = graph[v][j].second;
			if (d[v] + len < d[to])
			{
				set.erase(std::make_pair(d[to], to));
				d[to] = d[v] + len;
				p[to] = v;
				set.insert(std::make_pair(d[to], to));
			}
		}
	}
	for (int i = 0; i < n; i++)
	{
		std::cout << d[i] << " ";
	}
	std::cout << std::endl;
	return 0;
}