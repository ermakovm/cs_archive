﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace da2lab1F
{
    class Program
    {
        //***********************************************//
        const int MAXN = 100001;
        const int MAXM = 200001;
        const bool DEBUG = false;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static int N, M;
        static List<int>[] Edge;
        static bool[] used;
        static List<int> ans;
        /*static Stack<int> stack;
        static int[] Color;
        static List<int> ord;*/
        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            Thread mythread = new Thread(Th, 100000000);
            mythread.Start();
            mythread.Join();
            sr.Close();
            sw.Close();
        }

        static void Th()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "hamiltonian";
            resout = ".out";
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            N = tmp[0];
            M = tmp[1];
            //Color = new int[N + 2];
            Edge = new List<int>[N + 2];
            //ord = new List<int>(N + 2);
            for (int i = 0; i < Edge.Length; i++)
            {
                Edge[i] = new List<int>();
            }
            used = new bool[MAXN];
            ans = new List<int>();
            //stack = new Stack<int>();
        }

        static void solve()
        {
            for (int i = 0; i < M; i++)
            {
                int[] tmp = readInts();
                Edge[tmp[0] - 1].Add(tmp[1] - 1);
            }
            topsort();
            if (DEBUG)
            {
                for (int i = 0; i < N; i++)
                {
                    sw.Write((ans[i] + 1).ToString() + " ");
                }
            }
            for (int i = 0; i < N - 1; i++)
                if (ans[i] + 1 != ans[i + 1])
                {
                    sw.WriteLine("NO");
                    return;
                }
            sw.WriteLine("YES");
        }

        static void dfs(int v)
        {
            used[v] = true;
            for (int i = 0; i < Edge[v].Count; i++)
            {
                int to = Edge[v][i];
                if (!used[to])
                    dfs(to);
            }
            ans.Add(v);
        }

        static void topsort()
        {
            for (int i = 0; i < N; i++)
            {
                used[i] = false;
            }
            ans.Clear();
            for (int i = 0; i < N; i++)
            {
                if (!used[i])
                {
                    dfs(i);
                }
            }
            ans.Reverse();
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                if (DEBUG)
                {
                    sw.WriteLine(temp[i]);
                }
                res[i] = Convert.ToInt32(temp[i]);
            }
            return res;
        }

        static string[] readWords()
        {
            return sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
