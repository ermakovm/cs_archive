﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace da2lab2A
{
    class Program
    {
        //***********************************************//
        const int MAXN = 100000;
        const int MAXM = 200000;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static int n, m;
        static bool[] use;
        static List<int>[] graph;
        static List<int> components;
        static int count;
        static int[] res;
        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.Start();
            mythread.Join();
            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "components";
            resout = ".out";
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            n = tmp[0];
            m = tmp[1];
            graph = new List<int>[n];
            use = new bool[n];
            res = new int[n];
            count = 0;
            components = new List<int>();
            for (int i = 0; i < n; i++)
            {
                graph[i] = new List<int>();
            }
            for (int i = 0; i < m; i++)
            {
                tmp = readInts();
                int a = tmp[0] - 1;
                int b = tmp[1] - 1;
                graph[a].Add(b);
                graph[b].Add(a);                
            }
        }

        static void solve()
        {
            find();
            sw.WriteLine(count);
            for (int i = 0; i < n; i++)
                sw.Write(res[i] + " ");
        }

        static void dfs(int v)
        {
            use[v] = true;
            components.Add(v);
            for (int i = 0; i < graph[v].Count; i++)
            {
                int to = graph[v][i];
                if (!use[to])
                    dfs(to);
            }
        }

        static void find()
        {
            for (int i = 0; i < n; i++)
                use[i] = false;
            for (int i = 0; i < n; i++)
            {
                if (!use[i])
                {
                    components.Clear();
                    dfs(i);
                    count++;
                    for (int j = 0; j < components.Count; j++)
                        res[components[j]] = count;
                }
            }
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
            }
            return res;
        }

        static string[] readWords()
        {
            return sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
        }
    }
}
