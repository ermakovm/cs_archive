﻿#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace lab1A
{
    class Program
    {
        //***********************************************//

        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static string searchString, text, mainString, inverseString;
        static int[] z_result, z_inverse_result;
        //**********************************************//
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            mythread.Start();
#if DEBUG
            Console.WriteLine("Thread Start");
            sw.WriteLine("Thread Start");
#endif
            mythread.Join();
#if DEBUG
            Console.WriteLine("Thread Stop");
            sw.WriteLine("Thread Stop");
#endif

            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "search3";
#if DEBUG
            resout = ".debug";
#else
            resout = ".out";
#endif
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            searchString = readWords()[0];
            text = readWords()[0];
            mainString = new StringBuilder().Append(searchString).Append("#").Append(text).ToString();
            inverseString = new StringBuilder().Append(text).Append("#").Append(searchString).ToString();
        }



        static void solve()
        {
            z_result = z_function(mainString);
            z_inverse_result = iz_function(inverseString);
            if (searchString.Length > text.Length)
            {
                sw.WriteLine("0");
                return;
            }
            else
            {
                int n = z_result.Length;
                int[] new_mas = new int[n];
                List<int> res = new List<int>();

                for (int i = 0; i <= text.Length - searchString.Length; i++)
                {
                    if (z_result[i + searchString.Length + 1] + z_inverse_result[i + searchString.Length - 1] >= searchString.Length - 1)
                        res.Add(i + 1);
                }
                sw.WriteLine(res.Count);
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < res.Count; i++)
                    sb.Append(res[i].ToString()).Append(" ");
                sw.WriteLine(sb.ToString());
            }
        }

        static int[] iz_function(string pattern)
        {
            int n = pattern.Length - 1;
            int[] res = new int[pattern.Length];
            for (int i = n - 1, l = n, r = n; i >= 0; i--)
            {
                if (i >= l)
                    res[i] = Math.Min(i - l + 1, res[n - (r - i)]);
                while (i - res[i] >= 0 && pattern[n - res[i]] == pattern[i - res[i]])
                    res[i]++;
                if (i - res[i] + 1 < l)
                {
                    r = i;
                    l = i - res[i] + 1;
                }
            }
            return res;
        }

        static int[] z_function(string pattern)
        {
            int lenght = pattern.Length;
            int[] result = new int[lenght];
            int left = 0;
            int right = 0;
            for (int i = 1; i < lenght; i++)
            {
                if (i > right)
                {
                    int k = 0;
                    while (i + k < lenght && pattern[i + k] == pattern[k])
                        k++;
                    result[i] = k;
                    left = i;
                    right = i + k - 1;
                }
                else if (result[i - left] < right - i + 1)
                    result[i] = result[i - left];
                else
                {
                    int k = 1;
                    while (k + right < lenght && pattern[k + right - i] == pattern[right + k])
                        k++;
                    result[i] = right + k - i;
                    left = i;
                    right = right + k - 1;
                }
            }
            return result;
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
#if DEBUG
            Console.WriteLine("ReadInts");
            sw.WriteLine("ReadInts");
#endif
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
#if DEBUG
                Console.WriteLine(res[i]);
                sw.WriteLine(res[i]);
#endif
            }
            return res;
        }

        static string[] readWords()
        {
            string[] tmp = sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
#if DEBUG
            Console.WriteLine("ReadWord");
            sw.WriteLine("ReadWord");
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                sw.WriteLine(tmp[i]);
            }
#endif
            return tmp;
        }
    }

    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public void setPair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair(Pair<E, K> p)
        {
            this.first = p.getFirst();
            this.second = p.getSecond();
        }

        public void setFirst(E first = default(E))
        {
            this.first = first;
        }

        public void setSecond(K second = default(K))
        {
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

#if DEBUG
        public void printPair()
        {
            Console.WriteLine(getFirst() + " " + getSecond());
        }
#endif
    }
}