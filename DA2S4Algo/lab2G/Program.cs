﻿#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace lab2G
{
    class Program
    {
        //***********************************************//
        public const string alphabet = "abcdefghijklmnopqrstuvwxyz";
        public const int alphabetSize = 26;
        const int maxlen = 100000;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        public static char[] ch;
        static StreamReader sr;
        public static StreamWriter sw;
        //**********************************************//
        static string s, t;
        static State[] st;
        static int size, last, lastp;
        //**********************************************//
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            mythread.Start();
#if DEBUG
            Console.WriteLine("Thread Start");
            sw.WriteLine("Thread Start");
#endif
            mythread.Join();
#if DEBUG
            Console.WriteLine("Thread Stop");
            sw.WriteLine("Thread Stop");
#endif

            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "common";
#if DEBUG
            resout = ".debug";
#else
            resout = ".out";
#endif
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            s = readWords()[0];
            t = readWords()[0];

        }

        static void solve()
        {
            sw.WriteLine(lcs(s, t));
        }

        static void saExtend(char c)
        {
            int nlast = size++;
            st[nlast] = new State();
            st[nlast].length = st[last].length + 1;
            st[nlast].endpos = st[last].length;
            int p;
            for (p = last; p != -1 && st[p].next[c] == -1; p = st[p].link)
            {
                st[p].next[c] = nlast;
            }
            if (p == -1)
            {
                st[nlast].link = 0;
            }
            else
            {
                int q = st[p].next[c];
                if (st[p].length + 1 == st[q].length)
                    st[nlast].link = q;
                else
                {
                    int clone = size++;
                    st[clone] = new State();
                    st[clone].length = st[p].length + 1;
                    st[clone].next = (int[])st[q].next.Clone();
                    st[clone].link = st[q].link;
                    for (; p != -1 && st[p].next[c] != -1 && st[p].next[c] == q; p = st[p].link)
                        st[p].next[c] = clone;
                    st[q].link = clone;
                    st[nlast].link = clone;
                    st[clone].endpos = -1;
                }
            }
            last = nlast;
        }

        public static  void buildSA(String s)
        {
            int n = s.Length;
            st = new State[Math.Max(2, 2 * n - 1)];
            st[0] = new State();
            st[0].link = -1;
            st[0].endpos = -1;
            last = 0;
            size = 1;
            foreach (char x in s.ToCharArray())
            {
                saExtend(x);
            }
            for (int i = 1; i < size; i++)
            {
                st[st[i].link].ilink.Add(i);
            }
        }

        public static String lcs(String a, String b)
        {
            buildSA(a);
            int p = 0;
            lastp = 0;
            int len = 0;
            int best = 0;
            int bestpos = -1;
            for (int i = 0; i < b.Length; ++i)
            {
                char cur = b[i];
                if (st[p].next[cur] == -1)
                {
                    for (; p != -1 && st[p].next[cur] == -1; p = st[p].link)
                    {
                    }
                    if (p == -1)
                    {
                        p = 0;
                        len = 0;
                        continue;
                    }
                    len = st[p].length;
                }
                ++len;
                p = st[p].next[cur];
                if (best < len)
                {
                    best = len;
                    bestpos = i;
                    lastp = p;
                }
            }
            return b.Substring(bestpos - best + 1, bestpos + 1);
        }

        class State
        {
            public int length;
            public int link;
            public int endpos;
            public int[] next = new int[256];

            public State()
            {
                for (int i = 0; i < 256; i++)
                    next[i] = -1;
            }
            public List<int> ilink = new List<int>(0);
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
#if DEBUG
            Console.WriteLine("ReadInts");
            sw.WriteLine("ReadInts");
#endif
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
#if DEBUG
                Console.WriteLine(res[i]);
                sw.WriteLine(res[i]);
#endif
            }
            return res;
        }

        static string[] readWords()
        {
            string[] tmp = sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
#if DEBUG
            Console.WriteLine("ReadWord");
            sw.WriteLine("ReadWord");
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                sw.WriteLine(tmp[i]);
            }
#endif
            return tmp;
        }
    }

    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public void setPair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair(Pair<E, K> p)
        {
            this.first = p.getFirst();
            this.second = p.getSecond();
        }

        public void setFirst(E first = default(E))
        {
            this.first = first;
        }

        public void setSecond(K second = default(K))
        {
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

#if DEBUG
        public void printPair()
        {
            Console.WriteLine(getFirst() + " " + getSecond());
        }
#endif
    }
}