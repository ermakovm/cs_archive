﻿#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace lab2C
{
    class Program
    {
        //***********************************************//
        public const string alphabet = "abcdefghijklmnopqrstuvwxyz";
        public const int alphabetSize = 26;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        public static char[] ch;
        static StreamReader sr;
        public static StreamWriter sw;
        //**********************************************//
        static string inpstr;
        static int len;
        //**********************************************//
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            mythread.Start();
#if DEBUG
            Console.WriteLine("Thread Start");
            sw.WriteLine("Thread Start");
#endif
            mythread.Join();
#if DEBUG
            Console.WriteLine("Thread Stop");
            sw.WriteLine("Thread Stop");
#endif

            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "tree";
#if DEBUG
            resout = ".debug";
#else
            resout = ".out";
#endif
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            inpstr = readWords()[0];
            len = inpstr.Length;

            //sw.WriteLine(alphabet.Length);
        }

        static void solve()
        {
            SuffixTree tree = new SuffixTree(inpstr);
            tree.BuildTree();
            List<string> res=SuffixTree.Save(tree);
            for (int i = 0; i < res.Count; i++)
                sw.WriteLine(res[i]);
            //SuffixTree.Save(new BinaryWriter(File.Open("out.out", FileMode.Create)), tree);
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
#if DEBUG
            Console.WriteLine("ReadInts");
            sw.WriteLine("ReadInts");
#endif
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
#if DEBUG
                Console.WriteLine(res[i]);
                sw.WriteLine(res[i]);
#endif
            }
            return res;
        }

        static string[] readWords()
        {
            string[] tmp = sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
#if DEBUG
            Console.WriteLine("ReadWord");
            sw.WriteLine("ReadWord");
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                sw.WriteLine(tmp[i]);
            }
#endif
            return tmp;
        }
    }

    public class Node
    {
        public int suffixNode;

        public Node()
        {
            suffixNode = -1;
        }

        public static int Count = 1;
    }

    public class Suffix
    {
        public int originNode = 0;
        public int indexOfFirstCharacter;
        public int indexOfLastCharacter;

        public Suffix(string theString, Dictionary<int, Edge> edges, int node, int start, int stop)
        {
            this.originNode = node;
            this.indexOfFirstCharacter = start;
            this.indexOfLastCharacter = stop;
        }

        public bool IsExplicit
        {
            get
            {
                return indexOfFirstCharacter > indexOfLastCharacter;
            }
        }

        public void Canonize()
        {
            if (!IsExplicit)
            {
                Edge edge = Edge.Find(SuffixTree.theString, SuffixTree.Edges, originNode, SuffixTree.theString[indexOfFirstCharacter]);
                int edgeSpan = edge.indexOfLastCharacter - edge.indexOfFirstCharacter;
                while (edgeSpan <= (this.indexOfLastCharacter - this.indexOfFirstCharacter))
                {
                    this.indexOfFirstCharacter = this.indexOfFirstCharacter + edgeSpan + 1;
                    this.originNode = edge.endNode;
                    if (this.indexOfFirstCharacter <= this.indexOfLastCharacter)
                    {
                        edge = Edge.Find(SuffixTree.theString, SuffixTree.Edges, edge.endNode, SuffixTree.theString[this.indexOfFirstCharacter]);
                        edgeSpan = edge.indexOfLastCharacter - edge.indexOfFirstCharacter;
                    }
                }
            }
        }
    }

    public struct Edge
    {
        public int indexOfFirstCharacter;
        public int indexOfLastCharacter;
        public int startNode;
        public int endNode;

        public const int HASH_TABLE_SIZE = 306785407;

        public Edge(int startNode)
        {
            this.startNode = -1;
            this.indexOfFirstCharacter = 0;
            this.indexOfLastCharacter = 0;
            this.endNode = 0;
        }

        public Edge(string theString, int indexOfFirstCharacter, int indexOfLastCharacter, int parentNode)
        {
            this.indexOfFirstCharacter = indexOfFirstCharacter;
            this.indexOfLastCharacter = indexOfLastCharacter;
            this.startNode = parentNode;
            this.endNode = Node.Count++;
        }

        public void Copy(Edge edge)
        {
            this.startNode = edge.startNode;
            this.endNode = edge.endNode;
            this.indexOfFirstCharacter = edge.indexOfFirstCharacter;
            this.indexOfLastCharacter = edge.indexOfLastCharacter;
        }

        static public void Insert(Edge edge)
        {
            int i = Hash(edge.startNode, SuffixTree.theString[edge.indexOfFirstCharacter]);
            if (!SuffixTree.Edges.ContainsKey(i))
            {
                SuffixTree.Edges.Add(i, new Edge(-1));
            }
            while (SuffixTree.Edges[i].startNode != -1)
            {
                i = ++i % HASH_TABLE_SIZE;
                if (!SuffixTree.Edges.ContainsKey(i))
                {
                    SuffixTree.Edges.Add(i, new Edge(-1));
                }

            }
            SuffixTree.Edges[i] = edge;
        }

        static public void Remove(Edge edge)
        {
            int i = Hash(edge.startNode, SuffixTree.theString[edge.indexOfFirstCharacter]);
            while (SuffixTree.Edges[i].startNode != edge.startNode || SuffixTree.Edges[i].indexOfFirstCharacter != edge.indexOfFirstCharacter)
            {
                i = ++i % HASH_TABLE_SIZE;
            }
            for (; ; )
            {

                Edge tempEdge = SuffixTree.Edges[i];
                tempEdge.startNode = -1;
                SuffixTree.Edges[i] = tempEdge;
                int j = i;
                for (; ; )
                {
                    i = ++i % HASH_TABLE_SIZE;
                    if (!SuffixTree.Edges.ContainsKey(i))
                    {
                        SuffixTree.Edges.Add(i, new Edge(-1));
                    }
                    if (SuffixTree.Edges[i].startNode == -1)
                    {
                        return;
                    }

                    int r = Hash(SuffixTree.Edges[i].startNode, SuffixTree.theString[SuffixTree.Edges[i].indexOfFirstCharacter]);
                    if (i >= r && r > j)
                    {
                        continue;
                    }
                    if (r > j && j > i)
                    {
                        continue;
                    }
                    if (j > i && i >= r)
                    {
                        continue;
                    }
                    break;
                }
                SuffixTree.Edges[j].Copy(SuffixTree.Edges[i]);
            }
        }



        static public int SplitEdge(Suffix s, string theString, Dictionary<int, Edge> edges, Dictionary<int, Node> nodes, ref Edge edge)
        {
            Remove(edge);
            Edge newEdge = new Edge(theString, edge.indexOfFirstCharacter,
                edge.indexOfFirstCharacter + s.indexOfLastCharacter
                - s.indexOfFirstCharacter, s.originNode);
            Edge.Insert(newEdge);
            if (nodes.ContainsKey(newEdge.endNode))
            {
                nodes[newEdge.endNode].suffixNode = s.originNode;
            }
            else
            {
                Node newNode = new Node();
                newNode.suffixNode = s.originNode;
                nodes.Add(newEdge.endNode, newNode);
            }

            edge.indexOfFirstCharacter += s.indexOfLastCharacter - s.indexOfFirstCharacter + 1;
            edge.startNode = newEdge.endNode;
            Edge.Insert(edge);
            return newEdge.endNode;

        }

        static public Edge Find(string theString, Dictionary<int, Edge> edges, int node, int c)
        {
            int i = Hash(node, c);
            for (; ; )
            {
                if (!edges.ContainsKey(i))
                {
                    edges.Add(i, new Edge(-1));
                }
                if (edges[i].startNode == node)
                {
                    if (c == theString[edges[i].indexOfFirstCharacter])
                    {
                        return edges[i];
                    }
                }
                if (edges[i].startNode == -1)
                {
                    return edges[i];
                }
                i = ++i % HASH_TABLE_SIZE;
            }
        }

        public static int Hash(int node, int c)
        {
            int rtnValue = ((node << 8) + c) % (int)HASH_TABLE_SIZE;
            return rtnValue;
        }
    }

    public class SuffixTree
    {
        public static string theString;
        public static Dictionary<int, Edge> Edges = null;
        public static Dictionary<int, Node> Nodes = null;
        public SuffixTree(string theString)
        {
            SuffixTree.theString = theString;
            Nodes = new Dictionary<int, Node>();
            Edges = new Dictionary<int, Edge>();
        }

        public void BuildTree()
        {
            Suffix active = new Suffix(SuffixTree.theString, Edges, 0, 0, -1);
            for (int i = 0; i <= theString.Length - 1; i++)
            {
                AddPrefix(active, i);
            }
        }
        public static List<string> Save(SuffixTree tree)
        {           
            SortedSet<int> nodes = new SortedSet<int>();
            List<string>res=new List<string>();
            res.Add("");
            int edges = 0;
            foreach (KeyValuePair<int, Edge> edgePair in SuffixTree.Edges)
            {
                int a = edgePair.Value.startNode + 1;
                int b = edgePair.Value.endNode + 1;
                int c = edgePair.Value.indexOfFirstCharacter + 1;
                int d = edgePair.Value.indexOfLastCharacter + 1;
                if (a != 0)
                {
                    if (!nodes.Contains(a))
                        nodes.Add(a);
                    if (!nodes.Contains(b))
                        nodes.Add(b);
                    res.Add(a.ToString() + " " + b.ToString() + " " + c.ToString() + " " + d.ToString());
                    edges++;
                    
                }
                //Program.sw.WriteLine(edgePair.Value.endNode + " " + edgePair.Value.startNode + " " + edgePair.Value.indexOfFirstCharacter + " " + edgePair.Value.indexOfLastCharacter);
            }
            res[0] = nodes.Count.ToString() + " " + edges.ToString();
            return res;

        }

        private void AddPrefix(Suffix active, int indexOfLastCharacter)
        {
            int parentNode;
            int lastParentNode = -1;

            for (; ; )
            {
                Edge edge = new Edge(-1);
                parentNode = active.originNode;

                if (active.IsExplicit)
                {
                    edge = Edge.Find(SuffixTree.theString, SuffixTree.Edges, active.originNode, theString[indexOfLastCharacter]);
                    if (edge.startNode != -1)
                    {
                        break;
                    }
                }
                else
                {
                    edge = Edge.Find(SuffixTree.theString, SuffixTree.Edges, active.originNode, theString[active.indexOfFirstCharacter]);
                    int span = active.indexOfLastCharacter - active.indexOfFirstCharacter;
                    if (theString[edge.indexOfFirstCharacter + span + 1] == theString[indexOfLastCharacter])
                    {
                        break;
                    }
                    parentNode = Edge.SplitEdge(active, theString, Edges, Nodes, ref edge);
                }

                Edge newEdge = new Edge(SuffixTree.theString, indexOfLastCharacter, SuffixTree.theString.Length - 1, parentNode);
                Edge.Insert(newEdge);
                if (lastParentNode > 0)
                {
                    Nodes[lastParentNode].suffixNode = parentNode;
                }
                lastParentNode = parentNode;

                if (active.originNode == 0)
                {
                    active.indexOfFirstCharacter++;
                }
                else
                {
                    active.originNode = Nodes[active.originNode].suffixNode;
                }
                active.Canonize();
            }
            if (lastParentNode > 0)
            {
                Nodes[lastParentNode].suffixNode = parentNode;
            }
            active.indexOfLastCharacter++;
            active.Canonize();
        }
    }

    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public void setPair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair(Pair<E, K> p)
        {
            this.first = p.getFirst();
            this.second = p.getSecond();
        }

        public void setFirst(E first = default(E))
        {
            this.first = first;
        }

        public void setSecond(K second = default(K))
        {
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

#if DEBUG
        public void printPair()
        {
            Console.WriteLine(getFirst() + " " + getSecond());
        }
#endif
    }
}