﻿#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace lab2F
{
    class Program
    {
        //***********************************************//
        public const string alphabet = "abcdefghijklmnopqrstuvwxyz";
        public const int alphabetSize = 26;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        public static char[] ch;
        static StreamReader sr;
        public static StreamWriter sw;
        //**********************************************//
        static string inpstr;
        static int len;
        //**********************************************//
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            mythread.Start();
#if DEBUG
            Console.WriteLine("Thread Start");
            sw.WriteLine("Thread Start");
#endif
            mythread.Join();
#if DEBUG
            Console.WriteLine("Thread Stop");
            sw.WriteLine("Thread Stop");
#endif

            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "count";
#if DEBUG
            resout = ".debug";
#else
            resout = ".out";
#endif
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            inpstr = readWords()[0];
            len = inpstr.Length;

            //sw.WriteLine(alphabet.Length);
        }

        static void solve()
        {
            int[] sa = suffixArray(inpstr);
            int[] res = lcp(sa, inpstr);
            int n = inpstr.Length;
            int a = 0;
            for (int i = 0; i < n; i++)
            {
                a += (n - sa[i]);
            }
            int b = 0;
            for (int i = 0; i < n - 1; i++)
            {
                b += res[i];
            }
            sw.WriteLine(a - b);
        }

        public static int[] lcp(int[] sa, String s)
        {
            int n = sa.Length;
            int[] rank = new int[n];
            for (int i = 0; i < n; i++)
                rank[sa[i]] = i;
            int[] lcp = new int[n - 1];
            for (int i = 0, h = 0; i < n; i++)
            {
                if (rank[i] < n - 1)
                {
                    for (int j = sa[rank[i] + 1]; Math.Max(i, j) + h < s.Length && s[i + h] == s[j + h]; ++h)
                        ;
                    lcp[rank[i]] = h;
                    if (h > 0)
                        --h;
                }
            }
            return lcp;
        }

        public static int[] suffixArray(string str)
        {
            int n = str.Length;
            int[] sa = new int[n];
            int[] rank = new int[n];
            int maxlen = n;
            for (int i = 0; i < n; i++)
            {
                rank[i] = str[i];
                maxlen = Math.Max(maxlen, str[i] + 1);
            }

            int[] cnt = new int[maxlen];
            for (int i = 0; i < n; i++)
                ++cnt[rank[i]];
            for (int i = 1; i < cnt.Length; i++)
                cnt[i] += cnt[i - 1];
            for (int i = 0; i < n; i++)
                sa[--cnt[rank[i]]] = i;

            for (int len = 1; len < n; len *= 2)
            {
                int[] r = (int[])rank.Clone();
                rank[sa[0]] = 0;
                for (int i = 1; i < n; i++)
                {
                    int s1 = sa[i - 1];
                    int s2 = sa[i];
                    rank[s2] = r[s1] == r[s2] && Math.Max(s1, s2) + len < n && r[s1 + len / 2] == r[s2 + len / 2] ? rank[s1] : i;
                }
                for (int i = 0; i < n; i++)
                    cnt[i] = i;
                int[] s = (int[])sa.Clone();
                for (int i = 0; i < n; i++)
                {
                    int s1 = s[i] - len;
                    if (s1 >= 0)
                        sa[cnt[rank[s1]]++] = s1;
                }
            }
            return sa;
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
#if DEBUG
            Console.WriteLine("ReadInts");
            sw.WriteLine("ReadInts");
#endif
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
#if DEBUG
                Console.WriteLine(res[i]);
                sw.WriteLine(res[i]);
#endif
            }
            return res;
        }

        static string[] readWords()
        {
            string[] tmp = sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
#if DEBUG
            Console.WriteLine("ReadWord");
            sw.WriteLine("ReadWord");
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                sw.WriteLine(tmp[i]);
            }
#endif
            return tmp;
        }
    }

    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public void setPair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair(Pair<E, K> p)
        {
            this.first = p.getFirst();
            this.second = p.getSecond();
        }

        public void setFirst(E first = default(E))
        {
            this.first = first;
        }

        public void setSecond(K second = default(K))
        {
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

#if DEBUG
        public void printPair()
        {
            Console.WriteLine(getFirst() + " " + getSecond());
        }
#endif
    }
}