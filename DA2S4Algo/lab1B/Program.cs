﻿#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace lab1B
{
    class Program
    {
        //***********************************************//

        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        static StringBuilder sb;
        //**********************************************//
        static string p, t;
        static int m, n;
        //**********************************************//
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            mythread.Start();
#if DEBUG
            Console.WriteLine("Thread Start");
            sw.WriteLine("Thread Start");
#endif
            mythread.Join();
#if DEBUG
            Console.WriteLine("Thread Stop");
            sw.WriteLine("Thread Stop");
#endif

            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "search2";
#if DEBUG
            resout = ".debug";
#else
            resout = ".out";
#endif
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
            sb = new StringBuilder();
        }

        static void init()
        {
            p = readWords()[0];
            t = readWords()[0];
            n = p.Length;
            m = t.Length;
        }

        static void solve()
        {
            sb.Append(p).Append("#").Append(t);
            string news = sb.ToString();

            int[] pr = prefix(news);
#if DEBUG
            sw.WriteLine(news);
            for (int i = 0; i < pr.Length; i++)
            {
                sw.Write(pr[i] + " ");
            }
#endif
            int res = 0;
            List<int> resl = new List<int>(m);
            for (int i = n; i < pr.Length; i++)
            {
                if (pr[i] == n)
                {
                    res++;
                    resl.Add(i - 2 * n + 1);
                }
            }
            sw.WriteLine(res);
            for (int i = 0; i < res; i++)
                sw.Write(resl[i] + " ");
        }

        static int[] prefix(string pattern)
        {
            int p_l = pattern.Length;
            int[] result = new int[p_l];
            for (int i = 1; i < p_l; ++i)
            {
                int j = result[i - 1];
                while (j > 0 && pattern[i] != pattern[j])
                    j = result[j - 1];
                if (pattern[i] == pattern[j])
                    ++j;
                result[i] = j;
            }
            return result;
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
#if DEBUG
            Console.WriteLine("ReadInts");
            sw.WriteLine("ReadInts");
#endif
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
#if DEBUG
                Console.WriteLine(res[i]);
                sw.WriteLine(res[i]);
#endif
            }
            return res;
        }

        static string[] readWords()
        {
            string[] tmp = sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
#if DEBUG
            Console.WriteLine("ReadWord");
            sw.WriteLine("ReadWord");
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                sw.WriteLine(tmp[i]);
            }
#endif
            return tmp;
        }
    }

    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public void setPair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair(Pair<E, K> p)
        {
            this.first = p.getFirst();
            this.second = p.getSecond();
        }

        public void setFirst(E first = default(E))
        {
            this.first = first;
        }

        public void setSecond(K second = default(K))
        {
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

#if DEBUG
        public void printPair()
        {
            Console.WriteLine(getFirst() + " " + getSecond());
        }
#endif
    }
}