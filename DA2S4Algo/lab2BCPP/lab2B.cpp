#include <string>
#include <algorithm>
#include <fstream>
#include <cstdio>
#include <iostream>
#include <vector>

using namespace std;

//������� �������� � ������ - [0;L-1]
const int L = 400000;

//���������� ����������� �������
vector <int> getarr(string s)
{
	//s - �������� ������
	//���������� ������
	vector <int> arr;
	arr.resize(s.size());
	//������ ������
	vector <int> col;
	col.resize(s.size());
	//������ ��� ��������� ������
	vector <int> buf;
	buf.resize(s.size());
	//������ ��� �������� ����������
	vector <int> buck;
	buck.resize(max(L, (int)s.size()));
	//��� ������ - ��������� ����������
	//�� ����� ������������� ����� ������
	//��������� ���������� ���� ����
	for (int i = 0; i < (int)s.size(); i++)
		buck[s[i]]++;
	//����������� ������ ���, ����� ������ ������� �������� �� ��������� � ������� ������ ������ �����
	int sum = 0;
	for (int i = 0; i < L; i++)
		sum += buck[i], buck[i] = sum - buck[i];
	//������ �������� ������ arr: ������ � ��� �������� ������������� �� ������ �����
	for (int i = 0; i < (int)s.size(); i++)
		arr[buck[s[i]]++] = i;
	//������ ����������� �����: ���� ������������� �� 1 ���� ��������� ����� - ������
	col[arr[0]] = 0;
	for (int i = 1; i < (int)s.size(); i++)
		col[arr[i]] = col[arr[i - 1]] + (s[arr[i]] != s[arr[i - 1]]);
	int cn = col[arr[s.size() - 1]] + 1;
	//��� ������ - ����������� ���������� ��������
	//� ������ ����� ������������� ��������� ����� l, � � ����� - ����� 2l
	for (int l = 1; l < (int)s.size(); l *= 2)
	{
		//�������� ������ buck  � ��������� ��� ���������� �� col
		for (int i = 0; i < (int)s.size(); i++)
			buck[i] = 0;
		for (int i = 0; i < (int)s.size(); i++)
			buck[col[i]]++; sum = 0;
		for (int i = 0; i < cn; i++)
			sum += buck[i], buck[i] = sum - buck[i];
		//������ ����� ������ � buf (�� �������� �������� ��������� �� ������ �� l �����), ����� �������� ��� � arr
		for (int i = 0; i < (int)s.size(); i++)
			buf[buck[col[(arr[i] - l + s.size()) % s.size()]]++] = (arr[i] - l + s.size()) % s.size();
		arr = buf;
		//������ ������������� ������ col: ��������� ������ buf, ����������� ���� �� ������� ���� ���� �� ������ ����������, ����� ��������
		buf[arr[0]] = 0;
		for (int i = 1; i < (int)s.size(); i++)
			buf[arr[i]] = buf[arr[i - 1]] + (col[arr[i]] != col[arr[i - 1]] || col[(arr[i] + l) % s.size()] != col[(arr[i - 1] + l) % s.size()]);
		cn = buf[arr[s.size() - 1]] + 1;
		col = buf;
	}
	//���������� ���������
	return arr;
}

//���������� LCP
vector <int> getlcp(string s, vector<int> arr)
{
	//�������� ���������� ������
	vector <int> obr;
	obr.resize(s.size());
	//���������� ����� ������
	vector <int> lcp;
	lcp.resize(s.size());
	//������ �������� ���������� ������
	for (int i = 0; i < (int)s.size(); i++)
		obr[arr[i]] = i;
	//������� ��������
	int tek = 0;
	//i - ����� ��������, ���� �� ���������� ����� ��������
	for (int i = 0; i < (int)s.size() - 1; i++)
	{
		//���� ������� ��������� ��������� - ����������� �����
		while (s[i + tek] == s[arr[obr[i] + 1] + tek])
			tek++;
		//����������� lcp � ��������� tek �� 1
		lcp[obr[i]] = tek--;
		//��������� �� ����� �� �������
		if (tek < 0)
			tek = 0;
	}
	//���������� ���������
	return lcp;
}


int main(int argc, char**argv)
{
	ifstream input("array.in");
	cin.rdbuf(input.rdbuf());
	ofstream output("array.out");
	cout.rdbuf(output.rdbuf());
	string s;
	cin >> s;
	s += char(0);
	vector <int> arr = getarr(s); //lcp = getlcp(s, arr);
	vector<int>lcp = getlcp(s, arr);
	for (int i = 1; i < (int)s.size(); i++)
		cout << arr[i]+1 << ' ';

	
	cout << endl;

	input.close();
	output.close();
	return 0;
}