﻿#define DEBUG
#undef DEBUG
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;
using System.Collections;

namespace lab1G
{
    class Program
    {
        //***********************************************//
        const int maxn = 1000003;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static List<string> s;
        static string t;
        static int k;
        static Dictionary<string, bool> res = new Dictionary<string, bool>();
        //**********************************************//
        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.CurrentCulture = new System.Globalization.CultureInfo("en-US");
            mythread.Start();
#if DEBUG
            Console.WriteLine("Thread Start");
            sw.WriteLine("Thread Start");
#endif
            mythread.Join();
#if DEBUG
            Console.WriteLine("Thread Stop");
            sw.WriteLine("Thread Stop");
#endif

            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "search4";
#if DEBUG
            resout = ".debug";
#else
            resout = ".out";
#endif
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            k = readInts()[0];
            s = new List<string>(k);
            for (int i = 0; i < k; i++)
            {
                string str = readWords()[0];
                s.Add(str);
                if (!res.Keys.Contains(str))
                    res.Add(str, false);
            }
            t = readWords()[0];
        }

        static void solve()
        {
            AhoCorasic aho = new AhoCorasic();
            aho.Keywords = s;
            aho.FindAll(t, res);
            for (int i = 0; i < k; i++)
            {
                if (res[s[i]])
                    sw.WriteLine("YES");
                else
                    sw.WriteLine("NO");
            }
        }


        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
#if DEBUG
            Console.WriteLine("ReadInts");
            sw.WriteLine("ReadInts");
#endif
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
#if DEBUG
                Console.WriteLine(res[i]);
                sw.WriteLine(res[i]);
#endif
            }
            return res;
        }

        static string[] readWords()
        {
            string[] tmp = sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
#if DEBUG
            Console.WriteLine("ReadWord");
            sw.WriteLine("ReadWord");
            for (int i = 0; i < tmp.Length; i++)
            {
                Console.WriteLine(tmp[i]);
                sw.WriteLine(tmp[i]);
            }
#endif
            return tmp;
        }
    }

    public class AhoCorasic
    {
        class TreeNode
        {
            public TreeNode(TreeNode parent, char c)
            {
                this.c = c;
                this.parent = parent;
                arr_results = new ArrayList();
                resultsAr = new string[] { };
                transitionsAr = new TreeNode[] { };
                transHash = new Hashtable();
            }

            public void AddResult(string result)
            {
                if (arr_results.Contains(result)) return;
                arr_results.Add(result);
                resultsAr = (string[])arr_results.ToArray(typeof(string));
            }

            public void AddTransition(TreeNode node)
            {
                transHash.Add(node.Char, node);
                TreeNode[] ar = new TreeNode[transHash.Values.Count];
                transHash.Values.CopyTo(ar, 0);
                transitionsAr = ar;
            }

            public TreeNode GetTransition(char c)
            {
                return (TreeNode)transHash[c];
            }

            public bool ContainsTransition(char c)
            {
                return GetTransition(c) != null;
            }

            private char c;
            private TreeNode parent;
            private TreeNode flag;
            private ArrayList arr_results;
            private TreeNode[] transitionsAr;
            private string[] resultsAr;
            private Hashtable transHash;

            public char Char
            {
                get { return c; }
            }

            public TreeNode Parent
            {
                get { return parent; }
            }

            public TreeNode Failure
            {
                get { return flag; }
                set { flag = value; }
            }

            public TreeNode[] Transitions
            {
                get { return transitionsAr; }
            }

            public string[] Results
            {
                get { return resultsAr; }
            }

        }

        private TreeNode root;

        private List<string> keywords;

        public AhoCorasic(List<string> keywords)
        {
            Keywords = keywords;
        }

        public AhoCorasic()
        { }


        void BuildTree()
        {
            root = new TreeNode(null, ' ');
            foreach (string p in keywords)
            {
                TreeNode nd = root;
                foreach (char c in p)
                {
                    TreeNode ndNew = null;
                    foreach (TreeNode trans in nd.Transitions)
                        if (trans.Char == c) { ndNew = trans; break; }
                    if (ndNew == null)
                    {
                        ndNew = new TreeNode(nd, c);
                        nd.AddTransition(ndNew);
                    }
                    nd = ndNew;
                }
                nd.AddResult(p);
            }
            ArrayList nodes = new ArrayList();
            foreach (TreeNode nd in root.Transitions)
            {
                nd.Failure = root;
                foreach (TreeNode trans in nd.Transitions) nodes.Add(trans);
            }
            while (nodes.Count != 0)
            {
                ArrayList newNodes = new ArrayList();
                foreach (TreeNode nd in nodes)
                {
                    TreeNode r = nd.Parent.Failure;
                    char c = nd.Char;
                    while (r != null && !r.ContainsTransition(c)) r = r.Failure;
                    if (r == null)
                        nd.Failure = root;
                    else
                    {
                        nd.Failure = r.GetTransition(c);
                        foreach (string result in nd.Failure.Results)
                            nd.AddResult(result);
                    }
                    foreach (TreeNode child in nd.Transitions)
                        newNodes.Add(child);
                }
                nodes = newNodes;
            }
            root.Failure = root;
        }
        public List<string> Keywords
        {
            get { return keywords; }
            set
            {
                keywords = value;
                BuildTree();
            }
        }

        public void FindAll(string text,Dictionary<string,bool> res)
        {
            TreeNode ptr = root;
            int index = 0;
            while (index < text.Length)
            {
                TreeNode trans = null;
                while (trans == null)
                {
                    trans = ptr.GetTransition(text[index]);
                    if (ptr == root) break;
                    if (trans == null) ptr = ptr.Failure;
                }
                if (trans != null) ptr = trans;

                foreach (string found in ptr.Results)
                {
                    res[found] = true;
                }
                index++;
            }
        }
    }


    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public void setPair(E first = default(E), K second = default(K))
        {
            this.first = first;
            this.second = second;
        }

        public Pair(Pair<E, K> p)
        {
            this.first = p.getFirst();
            this.second = p.getSecond();
        }

        public void setFirst(E first = default(E))
        {
            this.first = first;
        }

        public void setSecond(K second = default(K))
        {
            this.second = second;
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

#if DEBUG
        public void printPair()
        {
            Console.WriteLine(getFirst() + " " + getSecond());
        }
#endif
    }
}