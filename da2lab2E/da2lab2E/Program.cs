﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.IO;

namespace da2lab2E
{
    class Program
    {
        //***********************************************//
        const int MAXN = 100000;
        const int MAXM = 200000;
        //***********************************************//
        static string filename;
        static string resout;
        static string resin;
        static char[] ch;
        static StreamReader sr;
        static StreamWriter sw;
        //**********************************************//
        static List<Pair<int, int>>[] graph;
        static int n, m, time, cur;
        static int[] colors, enter, ret;
        static bool[] used;
        static Stack<int> stack;
        static int count = 0;
        static List<int> res;
        //**********************************************//
        static void Main(string[] args)
        {
            initMain();
            Thread mythread = new Thread(ThreadRun, 100000000);
            mythread.Start();
            mythread.Join();
            sr.Close();
            sw.Close();
        }

        static void ThreadRun()
        {
            init();
            solve();
        }

        static void initMain()
        {
            filename = "bicone";
            resout = ".out";
            resin = ".in";
            ch = new char[] { ' ' };
            sr = new StreamReader(filename + resin);
            sw = new StreamWriter(filename + resout);
        }

        static void init()
        {
            int[] tmp = readInts();
            n = tmp[0];
            m = tmp[1];
            graph = new List<Pair<int, int>>[n];
            stack = new Stack<int>(n);
            enter = new int[n];
            ret = new int[n];
            used = new bool[n];
            colors = new int[n];
            res = new List<int>();
            for (int i = 0; i < n; i++)
            {
                graph[i] = new List<Pair<int, int>>();
                used[i] = false;
            }
            time = 0;
            cur = 0;
        }

        static void solve()
        {
            for (int i = 1; i <= m; i++)
            {
                int[] tmp = readInts();
                int a = tmp[0] - 1;
                int b = tmp[1] - 1;
                graph[a].Add(new Pair<int, int>(b, i));
                graph[b].Add(new Pair<int, int>(a, i));
            }
            for (int i = 0; i < n; i++)
            {
                if (!used[i])
                {
                    dfs(i, -1);
                }
            }
            cur = 0;
            for (int i = 0; i < n; i++)
                colors[i] = -1;
            for (int i = 0; i < n; i++)
            {
                if (colors[i] == -1)
                {
                    cur++;
                    paint(i, cur);
                }
            }
            sw.WriteLine(cur);
            for (int i = 0; i < n; i++)
                sw.Write(colors[i] + " ");
        }

        static void paint(int v, int color)
        {
            colors[v] = color;
            for (int i = 0; i < graph[v].Count; i++)
            {
                int u = graph[v][i].getFirst();
                if (colors[u] == -1)
                {
                    if (ret[u] == enter[u])
                    {
                        cur++;
                        paint(u, cur);
                    }
                    else
                    {
                        paint(u, color);
                    }
                }
            }
        }

        static void dfs(int v, int p)
        {
            used[v] = true;
            enter[v] = ret[v] = time++;
            for (int i = 0; i < graph[v].Count; i++)
            {
                int to = graph[v][i].getFirst();
                if (to == p)
                { continue; }
                if (used[to])
                    ret[v] = Math.Min(ret[v], enter[to]);
                else
                {
                    dfs(to, v);
                    ret[v] = Math.Min(ret[v], ret[to]);
                    if (ret[to] > enter[v])
                    {
                        cur++;
                        count++;
                        for (int k = 0; k < graph[v].Count; k++)
                        {
                            if (graph[v][k].getFirst() == to)
                            {
                                res.Add(graph[v][k].getSecond());
                                break;
                            }
                        }
                    }
                }
            }
        }

        static int[] readInts()
        {
            string[] temp = readWords();
            int[] res = new int[temp.Length];
            for (int i = 0; i < temp.Length; i++)
            {
                res[i] = Convert.ToInt32(temp[i]);
            }
            return res;
        }

        static string[] readWords()
        {
            return sr.ReadLine().Trim().Split(ch, StringSplitOptions.RemoveEmptyEntries);
        }
    }

    class Pair<E, K>
    {
        private E first;
        private K second;

        public Pair(E first, K second)
        {
            this.first = first;
            this.second = second;
        }

        public Pair()
        {
            this.first = default(E);
            this.second = default(K);
        }

        public E getFirst()
        {
            return first;
        }

        public K getSecond()
        {
            return second;
        }

        public void setFirst(E first)
        {
            this.first = first;
        }

        public void setSecond(K second)
        {
            this.second = second;
        }
    }
}
